package ru.t1.gorodtsova.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.t1.gorodtsova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.gorodtsova.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.gorodtsova.tm.client.AuthSoapEndpointClient;
import ru.t1.gorodtsova.tm.client.TaskSoapEndpointClient;
import ru.t1.gorodtsova.tm.marker.IntegrationCategory;
import ru.t1.gorodtsova.tm.model.Task;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@Category(IntegrationCategory.class)
public class TaskSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static ITaskRestEndpoint taskEndpoint;

    @NotNull
    private final Task task1 = new Task(UUID.randomUUID().toString());

    @NotNull
    private final Task task2 = new Task(UUID.randomUUID().toString());

    @NotNull
    private final Task task3 = new Task(UUID.randomUUID().toString());

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) taskEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void initTest() {
        taskEndpoint.save(task1);
        taskEndpoint.save(task2);
    }

    @After
    public void clean() {
        taskEndpoint.deleteAll();
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Test
    public void saveTest() {
        taskEndpoint.save(task3);
        Assert.assertNotNull(taskEndpoint.findById(task3.getId()));
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, taskEndpoint.findAll().size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String expected = task1.getId();
        @Nullable final Task task = taskEndpoint.findById(expected);
        Assert.assertNotNull(task);
        final String actual = task.getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void deleteAllTest() {
        taskEndpoint.deleteAll();
        Assert.assertNull(taskEndpoint.findAll());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = task1.getId();
        taskEndpoint.deleteById(id);
        Assert.assertNull(taskEndpoint.findById(id));
    }

    @Test
    public void deleteTest() {
        taskEndpoint.delete(task1);
        Assert.assertNull(taskEndpoint.findById(task1.getId()));
    }

}
