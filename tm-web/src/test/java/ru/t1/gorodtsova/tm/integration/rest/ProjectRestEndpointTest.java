package ru.t1.gorodtsova.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.gorodtsova.tm.marker.IntegrationCategory;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private static final HttpHeaders HEADERS = new HttpHeaders();

    @Nullable
    private static String sessionId;

    @NotNull
    private final Project project1 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project2 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project3 = new Project(UUID.randomUUID().toString());

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, Result.class, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        HEADERS.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<Project> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Project.class);
    }

    private static ResponseEntity<List> sendRequestList(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    @Before
    public void init() {
        @NotNull final String url = BASE_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, HEADERS));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project2, HEADERS));
    }

    @After
    public void clean() {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(HEADERS));
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(HEADERS));
    }

    @Test
    public void saveTest() {
        @NotNull final String saveUrl = BASE_URL + "save/";
        sendRequest(saveUrl, HttpMethod.POST, new HttpEntity<>(project3, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findById/" + project3.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

    @Test
    public void findAllTest() {
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(2, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String id = project1.getId();
        @NotNull final String url = BASE_URL + "findById/" + id;
        @NotNull final ResponseEntity<Project> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADERS));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Project project = response.getBody();
        Assert.assertNotNull(project);
        final String actual = project.getId();
        Assert.assertEquals(id, actual);
    }

    @Test
    public void deleteAllTest() {
        @NotNull final String deleteUrl = BASE_URL + "deleteAll/";
        sendRequest(deleteUrl, HttpMethod.POST, new HttpEntity<>(HEADERS));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = project1.getId();
        @NotNull final String url = BASE_URL + "deleteById/" + id;
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(HEADERS));
        @NotNull final String urlFind = BASE_URL + "findById/" + id;
        Assert.assertNull(sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

    @Test
    public void deleteTest() {
        @NotNull final String deleteUrl = BASE_URL + "delete/";
        sendRequest(deleteUrl, HttpMethod.POST, new HttpEntity<>(project2, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findById/" + project2.getId();
        Assert.assertNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

}
