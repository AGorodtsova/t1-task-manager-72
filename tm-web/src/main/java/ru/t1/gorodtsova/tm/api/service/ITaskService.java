package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create();

    void save(@Nullable Task model);

    void saveByUserId(@Nullable Task task, @Nullable String userId);

    void clear();

    void clearByUserId(@Nullable String userId);

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAllByUserId(@Nullable String userId);

    @Nullable
    Task findById(@Nullable String id);

    @Nullable
    Task findByIdAndUserId(@Nullable String id, @Nullable String userId);

    void remove(@Nullable Task model);

    void removeByUserId(@Nullable Task task, @Nullable String userId);

    void removeById(@Nullable String id);

    void removeByIdAndUserId(@Nullable String id, @Nullable String userId);

}
