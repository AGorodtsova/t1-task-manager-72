package ru.t1.gorodtsova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.gorodtsova.tm.api.service.IUserService;
import ru.t1.gorodtsova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RestController
@RequestMapping("api/users")
public class UserEndpointImpl {

    @Autowired
    private IUserService userService;

    @WebMethod
    @PostMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void save(
            @WebParam(name = "user", partName = "user")
            @RequestBody final User user
    ) {
        userService.save(user);
    }

    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<User> findAll() {
        return userService.findAll();
    }

    @WebMethod
    @GetMapping("/find/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return userService.findOneById(id);
    }

    @WebMethod
    @PostMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(
            @WebParam(name = "user", partName = "user")
            @RequestBody final User user
    ) {
        userService.remove(user);
    }

    @WebMethod
    @PostMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        userService.removeById(id);
    }

}
