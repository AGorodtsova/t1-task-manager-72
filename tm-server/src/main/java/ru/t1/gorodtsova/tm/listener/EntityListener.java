package ru.t1.gorodtsova.tm.listener;

import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.log.OperationEvent;
import ru.t1.gorodtsova.tm.log.OperationType;

@Component
public final class EntityListener implements PostInsertEventListener, PostUpdateEventListener, PostDeleteEventListener {

    @NotNull
    @Autowired
    private JmsLoggerProducer jmsLoggerProducer;

    @Override
    public void onPostDelete(@NotNull final PostDeleteEvent postDeleteEvent) {
        log(OperationType.DELETE, postDeleteEvent.getEntity());
    }

    @Override
    public void onPostInsert(@NotNull final PostInsertEvent postInsertEvent) {
        log(OperationType.INSERT, postInsertEvent.getEntity());
    }

    @Override
    public void onPostUpdate(@NotNull final PostUpdateEvent postUpdateEvent) {
        log(OperationType.UPDATE, postUpdateEvent.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(@NotNull final EntityPersister entityPersister) {
        return false;
    }

    public void log(@NotNull final OperationType operationType, @NotNull final Object entity) {
        jmsLoggerProducer.send(new OperationEvent(operationType, entity));
    }

}
