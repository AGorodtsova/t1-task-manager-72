package ru.t1.gorodtsova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.model.User;

@Repository
public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    Boolean existsByLogin(@NotNull String login);

    @NotNull
    Boolean existsByEmail(@NotNull String email);

}
